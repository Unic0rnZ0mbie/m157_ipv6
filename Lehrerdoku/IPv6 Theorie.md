﻿[TOC]
# IPv6 Theorie

Mit dem Internet kamen die IP Adressen hinzu. Die IPv4 Adressen mit 4‘300‘000‘000 Kombinationen schien enorm gross zu sein. Jedoch wurde bald erkennt, dass die Adressen doch nicht ausreichen und so wurde IPv6 entwickelt. Nun standen nicht mehr 32 Bit sondern 128 Bit als Adressraum zur Verfügung. Heute sind mehr als 20 Milliarden Geräte mit dem Internet verbunden. Sprich ist IPv4 bereits zu wenig.

![img](Pictures/IPV6Einfuehrung.PNG) 

Abbildung 1: Quelle:http://www.google.de/ipv6/  05.11.2019

 
IPv6 Vorteile: 

·         Adressraum mit 128 Bit (16 Byte) viel grösser. 

·         In nächster Zeit werden uns die IP’s nicht ausgehen 

·         „Internet of Things“ ist einfach realisierbar

·         Anzahl Adressen: 3.4*1038, das sind 340 Sextillionen 


Zusätzliche Vorteile sind:

•       Die vorderste Hexzahl definiert die Eigenschaften der IP (fe80:: lokal, 2001:: Netz, ...) 

•       Vereinfachte Adressierung (Header, Extended Header, Prefix vom Internet Provider, Interface Identifier 		des Gerätes) 

•       Die Adresse ist netzweit eindeutig, d.h. ohne NAT erreichbar.

•       "Uneingeschränkte" Punkt-zu-Punkt-Verbindung mit jedem Gerät. 

•       Weniger Netzbelastung durch wegfallen der NAT-Architektur 

•       Höhere Sicherheit durch Integration der IPSec (=Internet Protocol Security) 

 

Die Herausforderung mit IPv6 besteht darin, dass eine IPv6 Adresse komplett anders aufgebaut ist, als ein IPv4 Paket. Welches die Umstellung erschwert.


Am besten würden die Internet Service Provider mit Ankündigung in einer angegebenen Frist wie zB. 2 Jahre von IPv4 auf IPv6 umstellen. Dann müssten Haushälter und Betriebe, welche über deren DNS gehen diese Umstellung bis zu dem gewissen Datum realsieren. 

Ein guter Anfang ist der **Dual-Stack-Ansatz**. Hier werden beide Protokolle IPv4 und IPv6 parallel geführt. 

Somit kann man mögliche Probleme bei einer Umstellung bereits sehen und im Notfall auf IPv4 zurückgreifen.

Eine weitere Lösung sind **Tunnelmechanismen**. Da IPv4 und IPv6 Pakete nicht die gleichen Header Informationen beinhalten, würde bei einem Wechsel vom einen zum anderen Information verloren gehen. Aus diesem Grund müssen die Daten bei einem Protokollwechsel komplett, eingekapselt und durch den IPv4 Tunnel geschickt werden. Dieses Verfahren wird Tunneling genannt. Dabei werden IPv6 Pakete in den Nutzdaten von IPv4 Paketen zu einer Tunnelgegenstelle transportiert.

## IPv6 Zusammensetzung

Eine Änderung betrifft auch die Schreibweise der IPv6 Adressen. So werden acht Gruppen von je vier hexadezimalen Ziffern geschrieben, die durch Doppelpunkte getrennt werden. Zum Beispiel:  

2001:0db8:85a3:08d3:1319:8a2e:0370:7344 

**Adressnotation**: 

1. IPv6-Adressen werden gewöhnlicher weise hexadezimal notiert, wobei die Zahl in acht Blöcke zu jeweils 16 Bit (4 Hexadezimalstellen) unterteilt wird. Diese Blöcke werden durch Doppelpunkte (IPv4: Punkte) getrennt notiert:

   2001:0db8:85a3:08d3:1319:8a2e:0370:7344 

2. Führende Nullen innerhalb eines Blockes dürfen ausgelassen werden: 

    "2001:0db8:0000:08d3:0000:8a2e:0070:7344"  ist gleich wie: "2001:db8:0:8d3:0:8a2e:70:7344" 

3. Ein oder mehrere aufeinander folgende Blöcke, deren Wert *0* (bzw. *0000*) beträgt, dürfen ausgelassen werden. Dies wird durch zwei aufeinander folgende Doppelpunkte angezeigt: 

   "2001:0db8:0:0:0:0:1428:57ab"	ist gleich wie:	"2001:db8::1428:57ab"

4. Die Reduktion durch Regel 3 darf nur einmal durchgeführt werden, das heisst, es darf höchstens eine zusammenhängende Gruppe aus Null-Blöcken in der Adresse ersetzt werden. Die Adresse   "2001:0db8:0:0:8d3:0:0:0" darf demnach entweder zu "2001:db8:0:0:8d3::" oder "2001:db8::8d3:0:0:0"  gekürzt werden.

   "2001:db8::8d3::"  ist unzulässig, da dies mehrdeutig ist und fälschlicherweise z. B. auch als   "2001:db8:0:0:0:8d3:0:0" interpretiert werden könnte. Es empfiehlt sich den Block mit den meisten Null-Blöcken zu kürzen. 

5. Ebenfalls darf für die letzten vier Bytes (also 32 Bits) der Adresse die herkömmliche dezimale Notation verwendet werden. So ist "::ffff:127.0.0.1" eine alternative Schreibweise für "::ffff:7f00:1". Diese Schreibweise wird vor allem bei Einbettung des IPv4-Adressraums in den IPv6-Adressraum verwendet. 

   Sollen IPv6 Adressen als URL verwendet werden, müssen sie in eckige Klammern eingeschlossen werden. "http://[2001:0db8:85a3:08d3:1319:8a2e:0370:7344]"

   Dies verhindert dass Portnummern fälschlicherweise als Teil der IP angesehen werden. http://[2001:0db8:85a3:08d3:1319:8a2e:0370:7344]:8080/ 

 

## APIPA und spezielle IPv6 Adressen  

•       ::/128 zeigt das Fehlen einer Adresse an.  

•       ::1/128 ist gleichbedeutend wie 127.0.0.1 und steht für den Localhost.  

•       fe80::/10  Link-Local-Adressen sind nur innerhalb des lokalen Netzes gültig. Sie sind mit den APIPA Adressen (169.254.0.0/16) vergleichbar.                                

 

## IPv6 in der TBZ verwenden

Normalerweise gehen alle Setups davon aus, dass der Router Dual-Stack-Betrieb unterstützt. An der TBZ ist dies nicht der Fall. Sie können also nur mit der lokalen Adresse innerhalb des lokalen Sub-Netzes arbeiten, oder aber wie hier beschrieben ein IPv4-Tunneling aktivieren.



## IPv6 auf dem Raspberry Pi konfigurieren

Der Befehl *ifconfig* zeigt die IP Adresse des Raspberry Pi an. Dies ist standardmässig eine IPv4 Adresse. Natürlich unterstützt der Raspberry Pi auch IPv6. Dies ist neu seit Raspian Stretch automatisch aktiviert.
Die Ausgabe des Befehls *ifconfig* zeigt euch ob IPv6 aktiviert ist mit der Angabe der *inet6 Adresse*

Falls IPv6 nicht aktiviert sein sollte ist, können Sie es mit einem der folgenden Befehle tun:  

Temporär bis zum nächsten Reboot: 

`sudo modprobe ipv6`

Soll es länger aktiv sein, können Sie in `/etc/modules` einfach eine neue Zeile mit `ipv6` eintragen und den Raspberry Pi neu starten. 

Kontrollieren Sie (nach dem Aktivieren nochmals) mit *ifconfig*, ob nun eine IPv6 Adresse vorhanden ist. 
Diese ist unter **inet6** gelistet

Wie sie oben gesehen haben, ist die fe80:: Adresse nur eine lokale IPv6. 

Mit *ping6 ipv6.google.com*  

können sie testen, ob ein IPv6 ping funktioniert. Dies wird wahrscheinlich nicht funktionieren, da kein Router im TBZ-Netzwerk mit Dual-Stack operiert.        

Wollen wir den RasPi in das öffentliche IPv6 Netzwerk integrieren, müssen wir also einen Tunnel einrichten, welcher uns über einen Gateway mit dem IPv6-Internet verbindet.   

Es gibt mittlerweile eine menge Anbieter von IPv6/IPv4 Tunnels, hier eine Übersicht (der sich mit der Zeit verabschiedenden) IPv6-Tunnel-Broker: 

https://en.wikipedia.org/wiki/List_of_IPv6_tunnel_brokers 

 Wie in der Tabelle ersichtlich, werden für alle Dienste eine Registration benötigt.  

## Installation des HE IPv6 Tunnel Brokers 

Studieren Sie auch folgende Seite und erklären Sie die Funktionsweise des 6in4-Tunneling: 

http://www.elektronik-kompendium.de/sites/net/1904031.htm 

**Frage: Erklären sie 6in4-Tunneling mit eigenen Worten und zeigen sie es der Lehrperson für die Vervollständigung des Auftrags (Punkte für die Bewertung)**

*Lösung:*

*Die IPv6 Pakete werden einzeln abgekapselt und durch die normale Leitung mit IPv4 geschickt.* 
*Dies lässt zu, dass man mit einer IPv6 ins Internet gelangt, obwohl man im Netzwerk noch IPv4 benutzt.*


Machen Sie sich mit dem Dienst der Firma *HE* bekannt: 

https://www.tunnelbroker.net/ 

Ein HE-Tunnel für die TBZ ist bereits angemeldet! Sie müssen Sich also nicht registrieren! 

![img](Pictures/TunnelMain.png) 

Melden Sie sich an:
**Login: TBZ126** 

**PW: TbZ126!?** 

Öffnen Sie die Einstellung zum konfigurierten TBZ-Tunnel "M126-1": 

![img](Pictures/TunnelDetails.PNG) 

 

Die Beispielskonfiguration für die Datei`/etc/network/interfaces` könnten Sie nun hier ablesen, wobei wir noch eine Änderung machen müssen ... 

![img](Pictures/ExampleConfig.PNG) 

Wie hier unter "NOTE: ..." beschrieben müssen wir nicht die TBZ-IP, sondern die lokale (statische) IPv4-Adresse unseres RasPis angeben! Es wird automatisch ein Subnetz vom virtuellen Adapter *he-ipv6* erstellt. 

Folgende Anleitung soll ihnen ein Anhaltpunkt für die weitere Installation sein: 

**https://klenzel.de/5847**   **Genau vergleichen welche Einstellungen noch, bzw. anderst gemacht werden müssen! Wichtig ist dass die Reihenfolge der SSchritte eingehalten wird.** 

Dazu die konkreten Einstellungen:  

Als erstes mittels *ifconfig* die aktuelle, vom DHCP, verteile IP-Adresse auslesen.

ZZ ist die IP des aktuellen Subnetzes
XX ist ihre statische Geräteziffer, diese kann selber gewählt werden

## Netzwerkinterface Konfigurieren

#### sudo nano /etc/network/interfaces 


```iface eth0 inet manual

auto eth0
allow-hotplug eth0

**iface eth0 inet static**
address 10.71.**ZZ**.**XX**
netmask 255.255.255.0
gateway 10.71.**ZZ**.254

**iface eth0 inet6 static**
address 2001:470:**26**:218::**ZZ**
netmask 64

**auto he-ipv6**
iface he-ipv6 inet6 v4tunnel
address 2001:470:**25**:218::**2**
netmask 64
endpoint 216.66.80.98
#local 46.245.145.146
local 10.71.**ZZ**.XX
ttl 255
​gateway 2001:470:**25**:218::**1**
```

Nun sollte ping6 ipv6.google.com antworten

* Testen mit: traceroute6 -n ipv6.google.com 

* Jetzt sollten Sie die öffentliche IPv6 Adresse besitzen. Geben sie folgenden Befehl ein *ifconfig* und suchen den virtuellen Anschluss **he-ipv6**. Dort steht die konfigurierte IPv6-Adresse ihres Tunnels. Bei eth0 steht ihre eigene IPv6-Adresse 

* Oder via Testseite test-ipv6.com



## IPv6-Tunnel im Subnetz bekannt geben 

Momentan ist ihr Rasberry Pi noch das einzige Gerät, das eine IPv6-Adresse (getunnelt) hat. Praktisch wäre es nun, wenn alle im Subnetz eingebundenen Geräte den Tunnel verwenden könnten. Dazu müssen wir einen Dienst einrichten, der den Tunnel weitergibt. Studieren Sie die Seite https://en.wikipedia.org/wiki/Radvd 

**Frage: Wie funktioniert der Dienst?** 

*Lösung:*

*Link https://en.wikipedia.org/wiki/Radvd Abschnitt Daemon*

 

**Aufgabe: Installieren Sie radvd** **und öffnen Sie die Konfiguration des Dienstes**

*Lösung:*

*apt-get install radvd*

*sudo nano /etc/radvd.conf*: 
Die Konfiguration von radvd muss direkt im /etc Verzeichnis abgelegt werden. Es kann sein, dass der Service sagt, dass er die Konfigurations-Datei nicht finden kann. Erstellen sie diese.

### **Konfiguration des Dienstes:**

```interface eth0
{

AdvSendAdvert on;
AdvHomeAgentFlag off;
MinRtrAdvInterval 30;
MaxRtrAdvInterval 100;
AdvDefaultLifetime 9000;

prefix 2001:470:**26**:218::/64
​    {

AdvOnLink off;
AdvAutonomous on;
AdvRouterAddr on;
AdvValidLifetime infinity;
AdvPreferredLifetime infinity;
DeprecatePrefix on;

​	};

};
```
 

Starten und freigeben des Dienstes:

*sudo systemctl start radvd*
*sudo systemctl enable radvd*

 

Nun werden ALLE Geräte im IPv4-Subnetz mit einer dynamischen IPv6-Adresse (Prefix "2001:470:26:218::/64") beliefert und getunnelt! 

 

**Achtung**: Evtl. muss beim verwendeten Netzwerk-Adapter IPv6 zuerst freigegeben werden! Testen Sie evtl. mit Ethernet.

 

**Aufgabe: Mit Windows 10- CMD IP Anzeigen** *Lösung: ipconfig* 

![img](Pictures/IPconfigIPv6.PNG) 

Nun ist auch ihr Raspi vom Laptop aus erreichbar!

**Aufgabe: Pingen Sie mit ihrem Laptop *lokal* mal die IPv6-Adresse an.**

*Lösung:*

*Ping -6 "IPv6 Adresse" Z.B.: ping -6 2001:470:26:218::1* 

**SSH mit IPv6:** 

Eine *lokale* SSH Verbindung von Laptop auf den Raspberry Pi sollte nun ebenfalls über IPv6 möglich sein. Probieren Sie es aus (ohne eckigen Klammern!).  

Hinweis: Dies funktioniert nur wenn IPv6 permanent aktiviert ist und nicht mit *modprobe ipv6* --> sehen sie weiter oben im Workshop wie IPv6 permanent eingeschaltet werden kann.


```Falls sie nicht weiter machen, deaktivieren sie den radvd-Dienst wieder:
$ sudo systemctll stop radvd
$ sudo update-rc.d radvd disable
$ service --status-alle
```
Überprüfen sie, dass der Service radvd von einem: `[-]` angeführt wird

# DynDNS

**Aufgabe: Informieren sie sich im Internet über die Funktion eines DynDNS. Erklären sie der Lehrperson die Unterschiede zu einem normalen DNS**



Ist es uns zu mühsam immer die IPv6 Adresse einzugeben, besonders da diese jeweils nach dem Booten des RasPis ändert, können wir einen freien DNS Dienst beanspruchen.  z.B. afraid.org  

Hier können Sie sich registrieren und danach eine Domain auf ihre IP binden. 

Nach wenigen Minuten kann danach über die neue Domain die Webseite aufgerufen werden.   

Nun brauchen wir noch ein Script, welcher uns immer nach dem Booten die aktuelle IP einträgt. Afraid.com bietet im Register Dynamic DNS verschiedene Möglichkeiten an. So könnten wir zum Beispiel einfach eine Webseite aufrufen, welche unserem Benutzer zugeordnet ist und es wird automatisch die IP Adresse eingetragen. Dies funktioniert allerdings nicht, da der Aufruf via IPv4 Netz geschieht. Es würde nur die öffentliche IP der Firewall eingetragen.  

Öffnen Sie auf afraid.com im Register Dynamic DNS die vorgeschlagene Konfiguration für den Cron Job 

In dieser Konfiguration finden Sie die Zeilen 

\# Example #1: 

wget -O -

http://freedns.afraid.org/dynamic/update.php?*WHp5OFFnSk1LTEtrcXcZUj4bWN5WUc4OjE0MzcxODc2*  

Der KURSIVE Teil ist ihre verschlüsselte ID, welche sie anschliessend brauchen werden.  

Erstellen Sie in Ihrem Homeverzeichnis auf dem Raspberry Pi nun einen Ordner (mkdir) Scripts und erstellen Sie darin die Datei updatemyip.sh mit folgendem Scripinhalt.   Ergänzen Sie in Ihre ID. 



```MYIP=$(/sbin/ifconfig tun | grep inet6 | cut -d: -f2- | cut -d " " f2 | cut -d/ -f1)

URL="http://freedns.afraid.org/dynamic/update.php?WHp5OFFnSk1LTEtrcXcZUj4bWN5WUc4OjE0MzcxODc2&address="

URL1="$URL$MYIP" 
curl $URL1
```


Anschliessend müssen Sie die Datei noch mit *chmod 755 scripts/updatemyip.sh* ausführbar machen.  

 

## Cronjob einrichten 

 Mit crontab-e richten Sie einen Cronjob ein. Zum Crontab erhalten sie wenn sie folgenden Befehl eigeben:  

```
man crontab
```

**Frage: Welchen Vorteil bringt Cronjob?**

*Lösung:*

*Ein Cronjob kann Scripts automatisiert zu einer bestimmten Zeit eines bestimmten Tages ausführen.*
*Nun sollte Ihr RasPi bereits fünf Minuten nach dem Booten über ihre Subdomain zugänglich sein.*



![img](Pictures/Crontab.PNG) 


Der Nachteil dabei ist: Das Gerät von dem aus Sie auf den RasPi zugreifen wollen, muss ebenfals über IPv6 verfügen.  

 

**Frage: Welches Fazit könnte man aus der Aufgabe schliessen?**

*Musterbeispiel:*

*Fazit: Wenn Sie heute über IPv6 mit dem Internet verbunden sind kommen sie überall hin, mit IPv4 hingegen werden Sie zukünftig immer weniger Geräte erreichen können.*  